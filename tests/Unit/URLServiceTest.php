<?php

namespace Tests\Unit;

use App\Services\URLService;
use Goutte\Client as GoutteClient;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class URLServiceTest extends TestCase
{
    protected $service;

    const HTML = '<!doctype html>
                  <html>
                    <title>Test</title>
                    <meta name="description" content="Test">
                  </html>';

    public function setUp()
    {
        parent::setUp();

        $mock = new MockHandler([
            new Response(200, ['Content-type' => 'text/html'], self::HTML),
        ]);

        $httpClient = new GuzzleClient([
            'handler' => HandlerStack::create($mock),
        ]);

        $goutte = new GoutteClient();
        $goutte->setClient($httpClient);

        $this->service = new URLService();
        $this->service->setClient($goutte);
        $this->service->setURL('https://example.com');
    }
    /**
     * @test
     */
    public function shouldGetTitle()
    {
        $title = $this->service->getTitle();

        $this->assertEquals('Test', $title);
    }

    /**
     * @test
     */
    public function shouldGetDescription()
    {
        $description = $this->service->getDescription();

        $this->assertEquals('Test', $description);
    }

    /**
     * @test
     */
     public function shouldGetUrl()
     {
         $url = $this->service->getURL();

         $this->assertEquals('https://example.com', $url);
     }
}
