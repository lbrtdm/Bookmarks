<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;

class BookmarkApiTest extends TestCase
{
    /**
     * @test
     */
    public function canCreateABookmark()
    {
        $data = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'url' => $this->faker->url,
        ];

        $this->json('POST', route('bookmarks.store'), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }

    /**
     * @test
     */
    public function canCreateABookmarkGivenURLOnly()
    {
        $data = ['url' => 'https://example.com'];

        $this->json('POST', route('bookmarks.store'), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }

    /**
     * @test
     */
    public function shouldNotBePossibleToAddAnURLTwice()
    {
        $data = ['url' => 'https://example.com'];

        $this->json('POST', route('bookmarks.store'), $data)
            ->assertStatus(201)
            ->assertJson($data);

        $this->json('POST', route('bookmarks.store'), $data)
            ->assertStatus(400);
        
    }

    /**
     * @test
     */
    public function cannotCreateABookmarkWithMissingRequiredFields()
    {
        $data = [
            'url' => null,
        ];

        $this->json('POST', route('bookmarks.store'), $data)
            ->assertJsonStructure(['error'])
            ->assertStatus(400);
    }

    /**
     * @test
     */
    public function canEditABookmark()
    {
        $bookmark = factory(\App\Models\Bookmark::class)->create();
        $data = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'url' => $this->faker->url,
        ];

        $this->json('PUT', route('bookmarks.update',
            ['id' => $bookmark->id]), $data)
            ->assertStatus(200)
            ->assertJson($data);
    }

    /**
     * @test
     */
    public function canPartialEditABookmark()
    {
        $bookmark = factory(\App\Models\Bookmark::class)->create();
        $data = [
            'description' => $this->faker->paragraph,
        ];

        $this->json('PATCH', route('bookmarks.update',
            ['id' => $bookmark->id]), $data)
            ->assertStatus(200)
            ->assertJson($data);
    }

    /**
     * @test
     */
    public function canShowABookmark()
    {
        $bookmark = factory(\App\Models\Bookmark::class)->create();
        $this->json('GET', route('bookmarks.show', ['id' => $bookmark->id]))
            ->assertStatus(200)
            ->assertJson($bookmark->toArray());
    }

    /**
     * @test
     */
    public function cannotShowANonExistentBookmark()
    {
        $this->json('GET', route('bookmarks.show', ['id' => 1]))
            ->assertJsonStructure(['error'])
            ->assertStatus(404);
    }

    /**
     * @test
     */
    public function canShowAllBookmarks()
    {
        $bookmarks = factory(\App\Models\Bookmark::class, 10)->create();
        $this->json('GET', route('bookmarks.index'))
            ->assertStatus(200)
            ->assertJson($bookmarks->toArray());
    }

    /**
     * @test
     */
    public function canShowAllBookmarksOrderedByCreationDateDesc()
    {
        $bookmark1 = factory(\App\Models\Bookmark::class)->create();
        $bookmark2 = factory(\App\Models\Bookmark::class)->create();
        $bookmark1->created_at = Carbon::now()->addSeconds(20);
        $bookmark1->updated_at = Carbon::now()->addSeconds(20);
        $bookmark1->save();
        $bookmarks = [$bookmark1->toArray(), $bookmark2->toArray()];
        $response = $this->json('GET', route('bookmarks.index'));

        $response->assertStatus(200);
        $this->assertEquals($bookmarks, $response->original);
    }


    /**
     * @test
     */
    public function canDeleteABookmark()
    {
        $bookmark = factory(\App\Models\Bookmark::class)->create();
        $this->json('DELETE', route('bookmarks.destroy', ['id' => $bookmark->id]))
            ->assertStatus(204);
    }

    /**
     * @test
     */
    public function cannotDeleteANonExistentBookmark()
    {
        $this->json('DELETE', route('bookmarks.destroy', ['id' => 1]))
            ->assertJsonStructure(['error'])
            ->assertStatus(400);
    }
}
