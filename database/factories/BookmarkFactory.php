<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Bookmark::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->paragraph,
        'url' => $faker->url,
    ];
});
