<!doctype html>
<html lang="{{ app()->getLocale() }}" data-ng-app="app">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bookmarks</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    </head>
    <body>
        <div class="container-fluid">
            <!-- <div class="row"> -->
            <div class="row justify-content-md-center pt-5 pr-2 pl-2">
                <div class="col col-lg-2">
                </div>
                <div class="col-md">
                    <div class="" ui-view></div> <!-- This is where our views will load -->
                </div>
                <div class="col col-lg-2">
                </div>
            </div>
            <!-- </div> -->
         </div>
        <script src="{{ mix('js/bundle.js') }}"></script>
    </body>
</html>
