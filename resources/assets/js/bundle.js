
require('./bootstrap');

//load angular
require('angular');
require('angular-resource')
require('@uirouter/angularjs');

require('./app/app');
require('./app/app.config');

require('./app/bookmark/bookmark.module');
require('./app/bookmark/bookmark.service');
require('./app/bookmark/bookmark.controller');
