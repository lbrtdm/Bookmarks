// spec.js
describe('Bookmarks app', function() {
    it('should have a title', function() {
        browser.get(process.env.APP_URL);

        expect(browser.getTitle()).toEqual('Bookmarks');
    });

    it('should create a new bookmark', function() {
        browser.get(process.env.APP_URL);
        element(by.model('vm.bookmark.url')).sendKeys('http://example.net');
        element(by.id('add')).click().then(function() {
            browser.sleep(1000);
            var title = element(by.repeater('bookmark in vm.bookmarks').row(0).column('bookmark.title'));
            var description = element(by.repeater('bookmark in vm.bookmarks').row(0).column('bookmark.description'));
            var url = element(by.repeater('bookmark in vm.bookmarks').row(0).column('bookmark.url'));

            expect(title.getText()).toEqual('Example Domain');
            expect(description.getText()).toEqual('No description found');
            expect(url.getText()).toEqual('http://example.net');
        });
    });

});
