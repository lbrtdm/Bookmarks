(function() {
    'use strict';

    angular
        .module('app.bookmark')
        .factory('Bookmark', Bookmark);

    Bookmark.$inject = ['$resource', 'ENV'];

    function Bookmark($resource, ENV) {
        var params = {
            id: '@id'
        }

        var actions = {
            update: {
                method: 'PUT'
            }
        };

        var API_URL = ENV.API_URL + '/bookmarks/:id';

        return $resource(API_URL, params, actions);
    }

}());
