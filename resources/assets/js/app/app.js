(function() {
    'use strict';

    angular
        .module('app', [
            'ui.router',
            'ngResource',
            'app.bookmark'
        ])
        .constant('ENV', ENV);

}());
