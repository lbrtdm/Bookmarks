(function() {
    'use strict';

    angular.module('app').config(config).run(function($state) {
        $state.go('bookmarks');
    });;

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/bookmarks');

        $stateProvider
            .state('bookmarks', {
                url: '/bookmarks',
                templateUrl: 'views/bookmarks.html',
                controller: 'BookmarkController'
            })
            .state('bookmarksEdit', {
                url: '/bookmarks/:id/edit',
                templateUrl: 'views/bookmark.edit.html',
                controller: 'BookmarkController'
            });
    }

}());
