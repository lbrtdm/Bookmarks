<?php

namespace App\Exceptions\Bookmark;

class BookmarkNotFoundException extends \Exception
{
}
