<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\Translation\TranslatorInterface;
use App\Repositories\RepositoryInterface;
use App\Repositories\BookmarkRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            RepositoryInterface::class,
            BookmarkRepository::class
        );
        $this->app->bind('Symfony\Component\Translation\TranslatorInterface', function ($app) {
            return $app['translator'];
        });
    }
}
