<?php

namespace App\Repositories;

interface RepositoryInterface
{
    public function create(array $attributes);

    public function update(array $attributes, int $id);

    public function all($columns = ['*'], string $orderBy = 'id', string $sortBy = 'desc');

    public function find(int $id);

    public function findOneBy(array $data);

    public function delete(int $id);
}
