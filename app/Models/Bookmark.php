<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Bookmark Eloquent Model.
 *
 * @OAS\Schema(
 *     description="Bookmark model",
 *     title="Bookmark model",
 *     type="object",
 *     required={"url"},
 * )
 * @OAS\Property(property="id", type="integer")
 * @OAS\Property(property="title", type="string")
 * @OAS\Property(property="description", type="string")
 * @OAS\Property(property="url", type="string")
 */
class Bookmark extends Model
{
    protected $fillable = [
        'title',
        'description',
        'url',
    ];
}
