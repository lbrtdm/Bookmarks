<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BookmarkService;

/**
 * @OAS\Info(
 *     description="This is a sample Bookmarks API server.",
 *     version="1.0.0",
 *     title="Bookmarks",
 *     @OAS\Contact(
 *         email="lbrtdm@gmail.com"
 *     ),
 *     @OAS\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 * @OAS\Tag(
 *     name="bookmark",
 *     description="Everything about your Bookmarks",
 * )
 * @OAS\Server(
 *     description="Bookmarks API",
 *     url="http://bookmarks.test/api"
 * )
 */

class BookmarkApiController extends Controller
{
    /**
     * @var BookmarkService
     */
    private $service;

    /**
     * Class constructor.
     *
     * @param BookmarkService $service Bookmark service.
     */
    public function __construct(BookmarkService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @OAS\Get(
     *     path="/bookmarks",
     *     tags={"bookmark"},
     *     summary="Finds Bookmarks",
     *     description="Get all bookmarks ordered by creation date descendent",
     *     operationId="findBookmarks",
     *     @OAS\Response(
     *         response=200,
     *         description="successful operation",
     *         @OAS\MediaType(
     *             mediaType="application/json",
     *             @OAS\Schema(
     *                 type="array",
     *                 @OAS\Items(
     *                    ref="#/components/schemas/Bookmark"
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookmarks = $this->service->all();
        return response()->json($bookmarks, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OAS\RequestBody(
     *     request="Bookmark",
     *     description="Bookmark that needs to be added",
     *     required=true,
     *     @OAS\MediaType(
     *         mediaType="application/json",
     *         @OAS\Schema(
     *             ref="#/components/schemas/Bookmark"
     *         )
     *     )
     * )
     * @OAS\Post(
     *     path="/bookmarks",
     *     tags={"bookmark"},
     *     summary="Add a new bookmark",
     *     operationId="createBookmark",
     *     @OAS\Response(
     *         response=400,
     *         description="Invalid input"
     *     ),
     *     @OAS\Response(
     *         response=201,
     *         description="Successful operation",
     *     ),
     *     requestBody={"$ref": "#/components/requestBodies/Pet"}
     * )
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bookmark = $this->service->create($request->all());
        return response()->json($bookmark, 201);
    }

    /**
     * Display the specified resource.
     *
     * @OAS\Get(
     *     path="/bookmarks/{id}",
     *     tags={"bookmark"},
     *     summary="Find bookmark by ID",
     *     description="Returns a single boomark",
     *     operationId="getBookmark",
     *     @OAS\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of bookmark to return",
     *         required=true,
     *         @OAS\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OAS\Response(
     *         response=200,
     *         description="successful operation",
     *         @OAS\MediaType(
     *             mediaType="application/json",
     *             @OAS\Schema(
     *                 ref="#/components/schemas/Bookmark"
     *             ),
     *         )
     *     ),
     *     @OAS\Response(
     *         response=400,
     *         description="Invalid ID supplier"
     *     ),
     *     @OAS\Response(
     *         response=404,
     *         description="Bookmark not found"
     *     )
     * )
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bookmark = $this->service->find($id);

        if (!$bookmark) {
            return response(null, 404);
        }

        return response()->json($bookmark, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @OAS\Put(
     *     path="/bookmarks/{id}",
     *     tags={"bookmark"},
     *     summary="Update an existing bookmark",
     *     operationId="updateBookmark",
     *     @OAS\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of bookmark to return",
     *         required=true,
     *         @OAS\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OAS\Response(
     *         response=400,
     *         description="Validation error"
     *     ),
     *     requestBody={"$ref": "#/components/requestBodies/Pet"}
     * )
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bookmark = $this->service->update($request->all(), $id);
        return response()->json($bookmark, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OAS\Delete(
     *     path="/bookmarks/{id}",
     *     tags={"bookmark"},
     *     summary="Deletes a bookmark",
     *     operationId="deleteBookmark",
     *     @OAS\Parameter(
     *         name="id",
     *         in="path",
     *         description="Bookmark id to delete",
     *         required=true,
     *         @OAS\Schema(
     *             type="integer",
     *             format="int64"
     *         ),
     *     ),
     *     @OAS\Response(
     *         response=400,
     *         description="Invalid ID supplied",
     *     ),
     *     @OAS\Response(
     *         response=404,
     *         description="Pet not found",
     *     ),
     * )
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $isDeleted = $this->service->delete($id);
        if ($isDeleted) {
            return response()->json(null, 204);
        }
    }
}
