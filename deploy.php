<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'bookmarks');

// Project repository
set('repository', 'git@gitlab.com:lbrtdm/Bookmarks.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);


// Hosts

host('bookmarks.runciters.io')
    ->user('deployer')
    ->set('deploy_path', '/var/www/bookmarks.runciters.io');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

/**
 * Assets compilation
 */
task('assets:generate', function() {
    cd('{{release_path}}');
    run('yarn install');
    run('yarn run dev');
})->desc('Assets generation');

/**
 * API doc generation
 */
task('apidoc:generate', function() {
    run('{{bin/php}} {{release_path}}/artisan l5-swagger:generate');
});

// Reload PHP FPM service
task('reload:php-fpm', function () {
    run('sudo systemctl reload php7.2-fpm.service');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'assets:generate');
before('deploy:symlink', 'artisan:migrate');

// We need to reload PHP FPM after symlink creation
after('deploy:symlink', 'reload:php-fpm');

// API doc generation
after('deploy:symlink', 'apidoc:generate');
