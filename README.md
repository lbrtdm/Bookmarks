# Bookmarks

## A simple app (and RESTful API) to save your bookmarks

Live demo: https://bookmarks.runciters.io

CI & CD with Gitlab.com: https://gitlab.com/lbrtdm/Bookmarks/pipelines

API Documentation: https://bookmarks.runciters.io/api/documentation

### Setup

Install PHP dependencies
```
composer install

```

Install JavaScript & frontend dependencies
```
yarn install
```
or
```
npm install
```

PHPUnit Tests
```
./vendor/bin/phpunit
```

AngularJS app
```
yarn run dev
```

E2e test
```
yarn run e2e
```

Manual deploy
```
./vendor/bin/dep deploy
```
