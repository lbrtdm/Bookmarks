#!/bin/bash
# Update webdriver drivers
node ./node_modules/.bin/webdriver-manager update &&
# Start webdriver server (error messages to /dev/null)
node ./node_modules/.bin/webdriver-manager start 2>/dev/null &
# Wait 3 seconds for port 4444 to be ready
while ! nc -z 127.0.0.1 4444; do sleep 3; done
# Run protractor
APP_URL=https://bookmarks.test node ./node_modules/.bin/protractor resources/assets/js/e2e/conf.js
